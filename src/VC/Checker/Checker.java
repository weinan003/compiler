/**
 * Checker.java   
 * Sun Apr 24 15:57:55 AEST 2016
 **/

package VC.Checker;

import VC.ASTs.*;
import VC.Scanner.SourcePosition;
import VC.ErrorReporter;
import VC.StdEnvironment;

public final class Checker implements Visitor {

	private String errMesg[] = { "*0: main function is missing", "*1: return type of main is not int",

			// defined occurrences of identifiers
			// for global, local and parameters
			"*2: identifier redeclared", "*3: identifier declared void", "*4: identifier declared void[]",

			// applied occurrences of identifiers
			"*5: identifier undeclared",

			// assignments
			"*6: incompatible type for =", "*7: invalid lvalue in assignment",

			// types for expressions
			"*8: incompatible type for return", "*9: incompatible type for this binary operator",
			"*10: incompatible type for this unary operator",

			// scalars
			"*11: attempt to use an array/function as a scalar",

			// arrays
			"*12: attempt to use a scalar/function as an array", "*13: wrong type for element in array initialiser",
			"*14: invalid initialiser: array initialiser for scalar",
			"*15: invalid initialiser: scalar initialiser for array", "*16: excess elements in array initialiser",
			"*17: array subscript is not an integer", "*18: array size missing",

			// functions
			"*19: attempt to reference a scalar/array as a function",

			// conditional expressions in if, for and while
			"*20: if conditional is not boolean", "*21: for conditional is not boolean",
			"*22: while conditional is not boolean",

			// break and continue
			"*23: break must be in a while/for", "*24: continue must be in a while/for",

			// parameters
			"*25: too many actual parameters", "*26: too few actual parameters", "*27: wrong type for actual parameter",

			// reserved for errors that I may have missed (J. Xue)
			"*28: misc 1", "*29: misc 2",

			// the following two checks are optional
			"*30: statement(s) not reached", "*31: missing return statement", };

	private SymbolTable idTable;
	private static SourcePosition dummyPos = new SourcePosition();
	private ErrorReporter reporter;
	// Checks whether the source program, represented by its AST,
	// satisfies the language's scope rules and type rules.
	// Also decorates the AST as follows:
	// (1) Each applied occurrence of an identifier is linked to
	// the corresponding declaration of that identifier.
	// (2) Each expression and variable is decorated by its type.

	public Checker(ErrorReporter reporter) {
		this.reporter = reporter;
		this.idTable = new SymbolTable();
		establishStdEnvironment();
	}

	public void check(AST ast) {
		ast.visit(this, null);
	}

	// auxiliary methods

	private void declareVariable(Ident ident, Decl decl) {
		IdEntry entry = idTable.retrieveOneLevel(ident.spelling);

		if (entry != null)
			reporter.reportError(errMesg[2] + ": %", ident.spelling, dummyPos);
		idTable.insert(ident.spelling, decl);
	}

	// Programs

	public Object visitProgram(Program ast, Object o) {
		ast.FL.visit(this, null);
		String progEntrance = "main";
		int mFMiss = 0;
		int retNInt = 1;

		Decl mDecl = idTable.retrieve(progEntrance);
		if (mDecl == null || !mDecl.isFuncDecl())
			reporter.reportError(errMesg[mFMiss], "", dummyPos);
		else if (!mDecl.T.isIntType())
			reporter.reportError(errMesg[retNInt], "", dummyPos);

		return null;
	}

	// Statements

	public Object visitCompoundStmt(CompoundStmt ast, Object o) {
		Type ret;
		idTable.openScope();
		if (o instanceof FuncDecl)
			((FuncDecl) o).PL.visit(this, null);

		ast.DL.visit(this, null);
		ret = (Type) ast.SL.visit(this, o);

		idTable.closeScope();
		return ret;
	}

	public Object visitStmtList(StmtList ast, Object o) {
		Object ret1 = ast.S.visit(this, o);
		Object ret2 = ast.SL.visit(this, o);

		if (ast.S instanceof ReturnStmt && ast.SL instanceof StmtList)
			reporter.reportError(errMesg[30], "", ast.SL.position);

		return ast.S instanceof ReturnStmt ? ret1 : ret2;
	}

	public Object visitExprStmt(ExprStmt ast, Object o) {
		ast.E.visit(this, o);
		return null;
	}

	public Object visitEmptyStmt(EmptyStmt ast, Object o) {
		return null;
	}

	public Object visitEmptyStmtList(EmptyStmtList ast, Object o) {
		return null;
	}

	// Expressions

	// Returns the Type denoting the type of the expression. Does
	// not use the given object.

	public Object visitEmptyExpr(EmptyExpr ast, Object o) {
		ast.type = StdEnvironment.errorType;
		return ast.type;
	}

	public Object visitBooleanExpr(BooleanExpr ast, Object o) {
		ast.type = StdEnvironment.booleanType;
		return ast.type;
	}

	public Object visitIntExpr(IntExpr ast, Object o) {
		ast.type = StdEnvironment.intType;
		return ast.type;
	}

	public Object visitFloatExpr(FloatExpr ast, Object o) {
		ast.type = StdEnvironment.floatType;
		return ast.type;
	}

	public Object visitStringExpr(StringExpr ast, Object o) {
		ast.type = StdEnvironment.stringType;
		return ast.type;
	}

	public Object visitVarExpr(VarExpr ast, Object o) {
		ast.type = (Type) ast.V.visit(this, null);
		return ast.type;
	}

	// Declarations

	// Always returns null. Does not use the given object.

	public Object visitFuncDecl(FuncDecl ast, Object o) {
		IdEntry fun_id = idTable.retrieveOneLevel(ast.I.spelling);
		if (null != fun_id)
			reporter.reportError(errMesg[2], "", dummyPos);

		idTable.insert(ast.I.spelling, ast);
		Type retType = (Type) ast.S.visit(this, ast);
		// only retstmt may return unnull value in all stmt visit function
		if (!ast.T.isVoidType() && (retType == null || retType.isVoidType()))
			reporter.reportError(errMesg[31], "", dummyPos);

		return null;
	}

	public Object visitDeclList(DeclList ast, Object o) {
		ast.D.visit(this, null);
		ast.DL.visit(this, null);
		return null;
	}

	public Object visitEmptyDeclList(EmptyDeclList ast, Object o) {
		return null;
	}

	public Object visitGlobalVarDecl(GlobalVarDecl ast, Object o) {
		declareVariable(ast.I, ast);
		if (ast.T.isArrayType()) {
			ArrayType astArr = (ArrayType) ast.T;
			if (astArr.T.isVoidType())
				reporter.reportError(errMesg[4], "", dummyPos);
			else if (astArr.E.isEmptyExpr() && !(ast.E instanceof InitExpr))
				reporter.reportError(errMesg[18], "", dummyPos);
			else if (!ast.E.isEmptyExpr()) {
				Object rInt = ast.E.visit(this, ast);
				if (ast.E.type.isArrayType()) {
					Integer length = (Integer) rInt;
					if (astArr.E.isEmptyExpr())
						astArr.E = new IntExpr(new IntLiteral(length.toString(), dummyPos), dummyPos);
					else {
						Integer expectLength = Integer.parseInt(((IntExpr) astArr.E).IL.spelling);
						if (expectLength < length)
							reporter.reportError(errMesg[16], ast.I.spelling, dummyPos);
					}
				} else
					reporter.reportError(errMesg[15], ast.I.spelling, dummyPos);
			}
		} else {
			if (ast.T.isVoidType())
				reporter.reportError(errMesg[3], "", dummyPos);
			else {
				ast.E.visit(this, ast);
				if (ast.T.assignable(ast.E.type)) {
					if (!ast.T.equals(ast.E.type))
						ast.E = i2f(ast.E,ast);
				} else
					reporter.reportError(errMesg[6], "", dummyPos);
			}
		}
		return null;
	}

	public Object visitLocalVarDecl(LocalVarDecl ast, Object o) {
		declareVariable(ast.I, ast);
		if (ast.T.isArrayType()) {
			ArrayType astArr = (ArrayType) ast.T;
			if (astArr.T.isVoidType())
				reporter.reportError(errMesg[4], "", dummyPos);
			else if (astArr.E.isEmptyExpr() && !(ast.E instanceof InitExpr))
				reporter.reportError(errMesg[18], "", dummyPos);
			else if (!ast.E.isEmptyExpr()) {
				Object rInt = ast.E.visit(this, ast);
				if (ast.E.type.isArrayType()) {
					Integer length = (Integer) rInt;
					if (astArr.E.isEmptyExpr())
						astArr.E = new IntExpr(new IntLiteral(length.toString(), dummyPos), dummyPos);
					else {
						Integer expectLength = Integer.parseInt(((IntExpr) astArr.E).IL.spelling);
						if (expectLength < length)
							reporter.reportError(errMesg[16], ast.I.spelling, dummyPos);
					}
				} else
					reporter.reportError(errMesg[15], ast.I.spelling, dummyPos);
			}
		} else {
			if (ast.T.isVoidType())
				reporter.reportError(errMesg[3], "", dummyPos);
			else {
				ast.E.visit(this, ast);
				if (ast.T.assignable(ast.E.type)) {
					if (!ast.T.equals(ast.E.type))
						ast.E = i2f(ast.E,ast);
				} else
					reporter.reportError(errMesg[6], "", dummyPos);
			}
		}
		return null;
	}

	// Parameters

	// Always returns null. Does not use the given object.

	public Object visitParaList(ParaList ast, Object o) {
		ast.P.visit(this, null);
		ast.PL.visit(this, null);
		return null;
	}

	public Object visitParaDecl(ParaDecl ast, Object o) {
		declareVariable(ast.I, ast);

		if (ast.T.isVoidType()) {
			reporter.reportError(errMesg[3] + ": %", ast.I.spelling, dummyPos);
		} else if (ast.T.isArrayType()) {
			if (((ArrayType) ast.T).T.isVoidType())
				reporter.reportError(errMesg[4] + ": %", ast.I.spelling, dummyPos);
		}
		return null;
	}

	public Object visitEmptyParaList(EmptyParaList ast, Object o) {
		return null;
	}

	// Arguments

	// Your visitor methods for arguments go here

	// Types

	// Returns the type predefined in the standard environment.

	public Object visitErrorType(ErrorType ast, Object o) {
		return StdEnvironment.errorType;
	}

	public Object visitBooleanType(BooleanType ast, Object o) {
		return StdEnvironment.booleanType;
	}

	public Object visitIntType(IntType ast, Object o) {
		return StdEnvironment.intType;
	}

	public Object visitFloatType(FloatType ast, Object o) {
		return StdEnvironment.floatType;
	}

	public Object visitStringType(StringType ast, Object o) {
		return StdEnvironment.stringType;
	}

	public Object visitVoidType(VoidType ast, Object o) {
		return StdEnvironment.voidType;
	}

	// Literals, Identifiers and Operators

	public Object visitIdent(Ident I, Object o) {
		Decl binding = idTable.retrieve(I.spelling);
		if (binding != null)
			I.decl = binding;
		return binding;
	}

	public Object visitBooleanLiteral(BooleanLiteral SL, Object o) {
		return StdEnvironment.booleanType;
	}

	public Object visitIntLiteral(IntLiteral IL, Object o) {
		return StdEnvironment.intType;
	}

	public Object visitFloatLiteral(FloatLiteral IL, Object o) {
		return StdEnvironment.floatType;
	}

	public Object visitStringLiteral(StringLiteral IL, Object o) {
		return StdEnvironment.stringType;
	}

	public Object visitOperator(Operator O, Object o) {
		return null;
	}

	// Creates a small AST to represent the "declaration" of each built-in
	// function, and enters it in the symbol table.

	private FuncDecl declareStdFunc(Type resultType, String id, List pl) {

		FuncDecl binding;

		binding = new FuncDecl(resultType, new Ident(id, dummyPos), pl, new EmptyStmt(dummyPos), dummyPos);
		idTable.insert(id, binding);
		return binding;
	}

	// Creates small ASTs to represent "declarations" of all
	// build-in functions.
	// Inserts these "declarations" into the symbol table.

	private final static Ident dummyI = new Ident("x", dummyPos);

	private void establishStdEnvironment() {

		// Define four primitive types
		// errorType is assigned to ill-typed expressions

		StdEnvironment.booleanType = new BooleanType(dummyPos);
		StdEnvironment.intType = new IntType(dummyPos);
		StdEnvironment.floatType = new FloatType(dummyPos);
		StdEnvironment.stringType = new StringType(dummyPos);
		StdEnvironment.voidType = new VoidType(dummyPos);
		StdEnvironment.errorType = new ErrorType(dummyPos);

		// enter into the declarations for built-in functions into the table

		StdEnvironment.getIntDecl = declareStdFunc(StdEnvironment.intType, "getInt", new EmptyParaList(dummyPos));
		StdEnvironment.putIntDecl = declareStdFunc(StdEnvironment.voidType, "putInt", new ParaList(
				new ParaDecl(StdEnvironment.intType, dummyI, dummyPos), new EmptyParaList(dummyPos), dummyPos));
		StdEnvironment.putIntLnDecl = declareStdFunc(StdEnvironment.voidType, "putIntLn", new ParaList(
				new ParaDecl(StdEnvironment.intType, dummyI, dummyPos), new EmptyParaList(dummyPos), dummyPos));
		StdEnvironment.getFloatDecl = declareStdFunc(StdEnvironment.floatType, "getFloat", new EmptyParaList(dummyPos));
		StdEnvironment.putFloatDecl = declareStdFunc(StdEnvironment.voidType, "putFloat", new ParaList(
				new ParaDecl(StdEnvironment.floatType, dummyI, dummyPos), new EmptyParaList(dummyPos), dummyPos));
		StdEnvironment.putFloatLnDecl = declareStdFunc(StdEnvironment.voidType, "putFloatLn", new ParaList(
				new ParaDecl(StdEnvironment.floatType, dummyI, dummyPos), new EmptyParaList(dummyPos), dummyPos));
		StdEnvironment.putBoolDecl = declareStdFunc(StdEnvironment.voidType, "putBool", new ParaList(
				new ParaDecl(StdEnvironment.booleanType, dummyI, dummyPos), new EmptyParaList(dummyPos), dummyPos));
		StdEnvironment.putBoolLnDecl = declareStdFunc(StdEnvironment.voidType, "putBoolLn", new ParaList(
				new ParaDecl(StdEnvironment.booleanType, dummyI, dummyPos), new EmptyParaList(dummyPos), dummyPos));

		StdEnvironment.putStringLnDecl = declareStdFunc(StdEnvironment.voidType, "putStringLn", new ParaList(
				new ParaDecl(StdEnvironment.stringType, dummyI, dummyPos), new EmptyParaList(dummyPos), dummyPos));

		StdEnvironment.putStringDecl = declareStdFunc(StdEnvironment.voidType, "putString", new ParaList(
				new ParaDecl(StdEnvironment.stringType, dummyI, dummyPos), new EmptyParaList(dummyPos), dummyPos));

		StdEnvironment.putLnDecl = declareStdFunc(StdEnvironment.voidType, "putLn", new EmptyParaList(dummyPos));

	}

	@Override
	public Object visitEmptyExprList(EmptyExprList ast, Object o) {
		return null;
	}

	@Override
	public Object visitEmptyArgList(EmptyArgList ast, Object o) {
		// check real para list
		if (!(o instanceof EmptyParaList))
			reporter.reportError(errMesg[26], "", dummyPos);
		return null;
	}

	@Override
	public Object visitIfStmt(IfStmt ast, Object o) {
		ast.E.visit(this, o);
		if (ast.E.isEmptyExpr() || !ast.E.type.isBooleanType())
			reporter.reportError(errMesg[20], "", dummyPos);

		ast.S1.visit(this, o);
		ast.S2.visit(this, o);
		return null;
	}

	@Override
	public Object visitWhileStmt(WhileStmt ast, Object o) {
		ast.E.visit(this, o);
		ast.S.visit(this, o);
		if (ast.E.isEmptyExpr() || !ast.E.type.isBooleanType())
			reporter.reportError(errMesg[22], "", dummyPos);
		return null;
	}

	@Override
	public Object visitForStmt(ForStmt ast, Object o) {
		ast.E1.visit(this, o);
		ast.E2.visit(this, o);
		ast.E3.visit(this, o);
		ast.S.visit(this, o);
		if (!ast.E2.isEmptyExpr() && !ast.E2.type.isBooleanType())
			reporter.reportError(errMesg[21], "", dummyPos);
		return null;
	}

	@Override
	public Object visitBreakStmt(BreakStmt ast, Object o) {
		AST recallAST = ast.parent;
		while (recallAST != null) {
			if (recallAST instanceof WhileStmt || recallAST instanceof ForStmt)
				break;
			recallAST = recallAST.parent;
		}
		if (recallAST == null)
			reporter.reportError(errMesg[23], "", dummyPos);
		return null;
	}

	@Override
	public Object visitContinueStmt(ContinueStmt ast, Object o) {
		AST recallAST = ast.parent;
		while (recallAST != null) {
			if (recallAST instanceof WhileStmt || recallAST instanceof ForStmt)
				break;

			recallAST = recallAST.parent;
		}

		if (recallAST == null)
			reporter.reportError(errMesg[24], "", dummyPos);
		return null;
	}

	@Override
	public Object visitReturnStmt(ReturnStmt ast, Object o) {
		FuncDecl d = (FuncDecl) o;
		ast.E.visit(this, null);
		if (d.T.isVoidType()) {
			if (!ast.E.isEmptyExpr())
				reporter.reportError(errMesg[8], "", dummyPos);
		} else {
			if (ast.E.isEmptyExpr())
				reporter.reportError(errMesg[8], "", dummyPos);

			if (d.T.assignable(ast.E.type)) {
				if (!ast.E.type.equals(d.T))
					ast.E = i2f(ast.E,ast);
			} else {
				reporter.reportError(errMesg[8], "", dummyPos);
			}
		}
		return ast.E.type;
	}

	@Override
	public Object visitEmptyCompStmt(EmptyCompStmt ast, Object o) {
		return null;
	}

	@Override
	public Object visitUnaryExpr(UnaryExpr ast, Object o) {
		ast.E.visit(this, o);
		ast.type = ast.E.type;
		switch (ast.O.spelling) {
		case "+":
		case "-":
			if (ast.E.type.isIntType())
				ast.O.spelling = "i" + ast.O.spelling;
			else if (ast.E.type.isFloatType())
				ast.O.spelling = "f" + ast.O.spelling;
			else {
				reporter.reportError(errMesg[10], "", dummyPos);
				ast.type = StdEnvironment.errorType;
			}
			break;
		case "!":
			if (ast.E.type.isBooleanType())
				ast.O.spelling = "i" + ast.O.spelling;
			else {
				reporter.reportError(errMesg[10], "", dummyPos);
				ast.type = StdEnvironment.errorType;
			}
		}
		return null;
	}

	public Object visitBinaryExpr(BinaryExpr ast, Object o) {
		ast.E1.visit(this, null);
		ast.E2.visit(this, null);

		if (ast.E1.type.isFloatType() && ast.E2.type.isIntType())
			ast.E2 = i2f(ast.E2,ast);
		else if (ast.E1.type.isIntType() && ast.E2.type.isFloatType())
			ast.E1 = i2f(ast.E1,ast);
		
		if(ast.E1.type.isIntType() && ast.E2.type.isIntType())
			ast.type = StdEnvironment.intType;
		else if (ast.E1.type.isFloatType() && ast.E2.type.isFloatType())
			ast.type = StdEnvironment.floatType;
		else if (ast.E1.type.isBooleanType() && ast.E2.type.isBooleanType())
			ast.type = StdEnvironment.booleanType;
		else
			ast.type = StdEnvironment.errorType;
		
		switch (ast.O.spelling) {
		case ">":
		case "<":
		case ">=":
		case "<=":
			if(ast.type.isIntType() )
			{
				ast.type = StdEnvironment.booleanType;
				ast.O.spelling = "i" + ast.O.spelling;
			}
			else if(ast.type.isFloatType())
			{
				ast.type = StdEnvironment.booleanType;
				ast.O.spelling = "f" + ast.O.spelling;
			}
			else
				ast.type = StdEnvironment.errorType;
			break;
		case "&&":
		case "||": {
			if (!ast.type.isBooleanType())
				ast.type = StdEnvironment.errorType;
			break;
		}
		case "==":
		case "!=":
			if ( ast.type.isIntType() || ast.type.isBooleanType()) {
				ast.O.spelling = "i" + ast.O.spelling;
				ast.type = StdEnvironment.booleanType;
			} else if (ast.type.isFloatType()) {
				ast.O.spelling = "f" + ast.O.spelling;
				ast.type = StdEnvironment.booleanType;
			}
			else
				ast.type = StdEnvironment.errorType;
		}
		
		if(ast.type.isIntType())
			ast.O.spelling = "i" + ast.O.spelling;
		else if(ast.type.isFloatType())
			ast.O.spelling = "f" + ast.O.spelling;
		
		if(ast.type.isErrorType())
			reporter.reportError(errMesg[9], ast.O.spelling, dummyPos);
		return ast.type;
	}

	@Override
	public Object visitInitExpr(InitExpr ast, Object o) {
		Type declT = ((Decl) o).T;
		if (!declT.isArrayType()) {
			reporter.reportError(errMesg[14], "", dummyPos);
			ast.type = StdEnvironment.errorType;
			return 0;
		}
		ast.type = declT;
		return ast.IL.visit(this, declT);
	}

	@Override
	public Object visitExprList(ExprList ast, Object o) {
		ArrayType declT = (ArrayType) o;
		Type innerType = declT.T;
		ast.E.visit(this, null);
		if (innerType.assignable(ast.E.type)) {
			if (!innerType.equals(ast.E.type))
				ast.E = i2f(ast.E,ast);
		} else
			reporter.reportError(errMesg[13], "", dummyPos);

		if (ast.EL instanceof ExprList)
			((ExprList) ast.EL).index = ast.index + 1;
		Integer ret = (Integer) ast.EL.visit(this, o);
		return ret != null ? ret : ast.index + 1;
	}

	@Override
	public Object visitArrayExpr(ArrayExpr ast, Object o) {
		Type vType = (Type) ast.V.visit(this, null);
		Type eType = (Type) ast.E.visit(this, null);
		ast.type = StdEnvironment.errorType;
		if (vType.isArrayType())
			ast.type = ((ArrayType) vType).T;
		else if (!vType.isErrorType())
			reporter.reportError(errMesg[12], "", dummyPos);

		if (!eType.isIntType())
			reporter.reportError(errMesg[17], "", dummyPos);
		return ast.type;
	}

	@Override
	public Object visitCallExpr(CallExpr ast, Object o) {
		Decl d = idTable.retrieve(ast.I.spelling);
		ast.type = StdEnvironment.errorType;
		if (d == null) {
			reporter.reportError(errMesg[5], ast.I.spelling, dummyPos);
		} else {
			if (d.isFuncDecl()) {
				// do matching formal parameters type
				ast.AL.visit(this, ((FuncDecl) d).PL);
				ast.type = ((FuncDecl) d).T;
			} else
				reporter.reportError(errMesg[19], ast.I.spelling, dummyPos);
		}
		return ast.type;
	}

	@Override
	public Object visitAssignExpr(AssignExpr ast, Object o) {
		// lvalue begin
		ast.E1.visit(this, o);
		if (ast.E1 instanceof VarExpr) {
			SimpleVar v = (SimpleVar) ((VarExpr) ast.E1).V;
			Decl decl = idTable.retrieve(v.I.spelling);
			if (decl instanceof FuncDecl) {
				reporter.reportError(errMesg[7], "", dummyPos);
				ast.type = StdEnvironment.errorType;
			}
		} else if (!(ast.E1 instanceof ArrayExpr)) {
			reporter.reportError(errMesg[7], "", dummyPos);
			ast.type = StdEnvironment.errorType;
		}
		// lvalue done

		// rvalue begin
		ast.E2.visit(this, o);
		if (ast.E1.type.assignable(ast.E2.type)) {
			if (!ast.E1.type.equals(ast.E2.type)) {
				ast.E2 = i2f(ast.E2,ast);
				ast.type = StdEnvironment.floatType;
			} else
				ast.type = ast.E1.type;
		} else {
			reporter.reportError(errMesg[6], "", dummyPos);
			ast.type = StdEnvironment.errorType;
		}
		return null;
	}

	@Override
	public Object visitArgList(ArgList ast, Object o) {
		List paraList = (List) o;

		if (paraList.isEmptyParaList())
			reporter.reportError(errMesg[25], " ", dummyPos);
		else {
			ast.A.visit(this, ((ParaList) paraList).P);
			ast.AL.visit(this, ((ParaList) paraList).PL);
		}
		return null;
	}

	@Override
	public Object visitArg(Arg ast, Object o) {
		Type dType = ((Decl) o).T;
		Type eType = (Type) ast.E.visit(this, null);
		if(ast.E instanceof UnaryExpr)
			eType = ((UnaryExpr)ast.E).E.type;
		if(ast.E instanceof AssignExpr)
			eType = ast.E.type;
		if (dType.assignable(eType)) {
			if (!dType.equals(eType))
				ast.E = i2f(ast.E,ast);
		} else {
			if (dType.isArrayType() && eType.isArrayType()) {
				Type dinnerType = ((ArrayType) dType).T;
				Type einnerType = ((ArrayType) eType).T;
				if (!dinnerType.equals(einnerType)) {
					reporter.reportError(errMesg[27], "", dummyPos);
					ast.type = StdEnvironment.errorType;
				}
			} else {
				reporter.reportError(errMesg[27], "", dummyPos);
				ast.type = StdEnvironment.errorType;
			}
		}
		return ast.type;
	}

	@Override
	public Object visitArrayType(ArrayType ast, Object o) {
		return ast;
	}

	@Override
	public Object visitSimpleVar(SimpleVar ast, Object o) {
		Decl d = idTable.retrieve(ast.I.spelling);
		if (d == null) {
			reporter.reportError(errMesg[5], ast.I.spelling, dummyPos);
			ast.type = StdEnvironment.errorType;
		} else if ((d.T.isArrayType() || d.isFuncDecl()) && ast.parent instanceof VarExpr
				&& !(ast.parent.parent instanceof Arg)) {
			reporter.reportError(errMesg[11], ast.I.spelling, dummyPos);
			ast.type = StdEnvironment.errorType;
		} else {
			ast.type = d.T;
		}

		return ast.type;
	}

	Expr i2f(Expr expr,AST parent) {
		Operator op = new Operator("i2f", dummyPos);
		Expr ret = new UnaryExpr(op, expr, dummyPos);
		ret.type = StdEnvironment.floatType;
		ret.parent = parent;
		return ret;
	}

}
