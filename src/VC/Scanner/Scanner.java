/**
 **	Scanner.java                        
 **/

package VC.Scanner;

import VC.ErrorReporter;

public final class Scanner {

    private SourceFile sourceFile;
    private boolean debug;

    private ErrorReporter errorReporter;
    private StringBuffer currentSpelling;
    private char currentChar;
    private SourcePosition sourcePos;

    public Scanner(SourceFile source, ErrorReporter reporter) {
        sourceFile = source;
        errorReporter = reporter;
        currentChar = sourceFile.getNextChar();
        debug = false;

        /* initialize currentLine and currentColumn for Token Position */
        sourcePos = new SourcePosition(1, 1, 0);
    }

    public void enableDebugging() {
        debug = true;
    }

    // accept gets the next character from the source program.

    private void accept() {
        currentSpelling.append(currentChar);
        sourcePos.charFinish++;
        currentChar = sourceFile.getNextChar();
    }

    // inspectChar returns the n-th character after currentChar
    // in the input stream.
    //
    // If there are fewer than nthChar characters between currentChar
    // and the end of file marker, SourceFile.eof is returned.
    //
    // Both currentChar and the current position in the input stream
    // are *not* changed. Therefore, a subsequent call to accept()
    // will always return the next char after currentChar.

    private char inspectChar(int nthChar) {
        return sourceFile.inspectChar(nthChar);
    }

    private int nextToken() {
        // Tokens: separators, operators, literals, identifiers and keyworods

        switch (currentChar) {
            case '+':
                accept();
                return Token.PLUS;
            case '-':
                accept();
                return Token.MINUS;
            case '*':
                accept();
                return Token.MULT;
            case '/':
                accept();
                return Token.DIV;
            case '!':
                accept();
                if ('=' == currentChar) {
                    accept();
                    return Token.NOTEQ;
                } else
                    return Token.NOT;
            case '=':
                accept();
                if ('=' == currentChar) {
                    accept();
                    return Token.EQEQ;
                } else
                    return Token.EQ;
            case '<':
                accept();
                if ('=' == currentChar) {
                    accept();
                    return Token.LTEQ;
                }

                else
                    return Token.LT;
            case '>':
                accept();
                if ('=' == currentChar) {
                    accept();
                    return Token.GTEQ;
                } else
                    return Token.GT;
            case '&':
                accept();
                if ('&' == currentChar)
                {
                	accept();
                    return Token.ANDAND;
                }
                else
                    return Token.ERROR;
            case '|':
                accept();
                if (currentChar == '|') {
                    accept();
                    return Token.OROR;
                } else {
                    return Token.ERROR;
                }
            case '{':
                accept();
                return Token.LCURLY;
            case '}':
                accept();
                return Token.RCURLY;
            case '(':
                accept();
                return Token.LPAREN;
            case ')':
                accept();
                return Token.RPAREN;
            case '[':
                accept();
                return Token.LBRACKET;
            case ']':
                accept();
                return Token.RBRACKET;
            case ';':
                accept();
                return Token.SEMICOLON;
            case ',':
                accept();
                return Token.COMMA;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                absorbInt();

                if (currentChar == '.') {
                    absorbFraction();
                    if ((currentChar == 'e' || currentChar == 'E') &&
                            (((inspectChar(1) == '+' || inspectChar(1) == '-')
                                    && Character.isDigit(inspectChar(2))) ||
                                    Character.isDigit(inspectChar(1))))
                        absorbExponent();

                    return Token.FLOATLITERAL;
                } else if ((currentChar == 'e' || currentChar == 'E') &&
                        (((inspectChar(1) == '+' || inspectChar(1) == '-')
                                && Character.isDigit(inspectChar(2))) ||
                                Character.isDigit(inspectChar(1)))) {
                    absorbExponent();
                    return Token.FLOATLITERAL;
                }
                return Token.INTLITERAL;
            case '.':
                if (Character.isDigit(inspectChar(1))) {
                    absorbFraction();
                    if ((currentChar == 'e' || currentChar == 'E') &&
                            (((inspectChar(1) == '+' || inspectChar(1) == '-')
                                    && Character.isDigit(inspectChar(2))) ||
                                    Character.isDigit(inspectChar(1))))
                        absorbExponent();
                    return Token.FLOATLITERAL;
                }
                accept();
                return Token.ERROR;
            case '"':
                /* ignore double quote */
                currentChar = sourceFile.getNextChar();
                sourcePos.charFinish++;

                /* accept legal character until meet eof or back quote */
                while (currentChar != '"') {
                    if (currentChar == '\\') {
                        switch (inspectChar(1)) {
                            case 'b':
                                sourceFile.getNextChar();
                                currentChar = sourceFile.getNextChar();
                                currentSpelling.append('\b');
                                sourcePos.charFinish += 2;
                                break;
                            case 'f':
                                sourceFile.getNextChar();
                                currentChar = sourceFile.getNextChar();
                                currentSpelling.append('\f');
                                sourcePos.charFinish += 2;
                                break;
                            case 'n':
                                sourceFile.getNextChar();
                                currentChar = sourceFile.getNextChar();
                                currentSpelling.append('\n');
                                sourcePos.charFinish += 2;
                                break;
                            case 'r':
                                sourceFile.getNextChar();
                                currentChar = sourceFile.getNextChar();
                                currentSpelling.append('\r');
                                sourcePos.charFinish += 2;
                                break;
                            case 't':
                                sourceFile.getNextChar();
                                currentChar = sourceFile.getNextChar();
                                currentSpelling.append('\t');
                                sourcePos.charFinish += 2;
                                break;
                            case '\\':
                                sourceFile.getNextChar();
                                currentChar = sourceFile.getNextChar();
                                currentSpelling.append('\\');
                                sourcePos.charFinish += 2;
                                break;
                            case '\'':
                                sourceFile.getNextChar();
                                currentChar = sourceFile.getNextChar();
                                currentSpelling.append('\'');
                                sourcePos.charFinish += 2;
                                break;
                            case '\"':
                                sourceFile.getNextChar();
                                currentChar = sourceFile.getNextChar();
                                currentSpelling.append('\"');
                                sourcePos.charFinish += 2;
                                break;
                            default:
                                sourceFile.getNextChar();
                                currentChar = sourceFile.getNextChar();
                                sourcePos.charFinish += 2;
                                errorReporter.reportError(
                                        "\\" + currentChar + ": Illegal escape character", null,
                                        new SourcePosition(sourcePos.lineStart,
                                                sourcePos.charFinish - 1, sourcePos.charFinish));
                        }
                    } else if (currentChar == SourceFile.eof || currentChar == '\n') {
                        errorReporter.reportError(currentSpelling + ": Unterminated string", null,
                                new SourcePosition(sourcePos.lineStart, sourcePos.charStart,
                                        sourcePos.charStart));

                        /*
                         * although the string need a back quote finish define
                         * the string, it also consider as a string after pose
                         * the error to user
                         */
                        return Token.STRINGLITERAL;
                    } else
                        accept();
                }

                currentChar = sourceFile.getNextChar();
                sourcePos.charFinish++;
                return Token.STRINGLITERAL;
            case SourceFile.eof:
                currentSpelling.append('$');
                sourcePos.charFinish++;
                currentChar = sourceFile.getNextChar();
                return Token.EOF;
            default:
                if (Character.isLetter(currentChar) || currentChar == '_') {
                    do {
                        accept();
                    } while (Character.isLetter(currentChar) || currentChar == '_'
                            || Character.isDigit(currentChar));

                    for (int i = Token.BOOLEAN; i < Token.PLUS; i++) {
                        if (Token.spell(i).equals(currentSpelling))
                            return i;
                    }

                    if ("true".equals(currentSpelling.toString())
                            || "false".equals(currentSpelling.toString()))
                        return Token.BOOLEANLITERAL;

                    return Token.ID;
                }
                break;
        }

        accept();
        return Token.ERROR;
    }

    /* accept pure digit */
    private boolean absorbInt() {
        boolean ret = false;
        while (Character.isDigit(currentChar)) {
            ret = true;
            accept();
        }

        return ret;
    }

    /* accept fraction mode digit */
    private void absorbFraction() {
        if (currentChar == '.') {
            accept();
            while (Character.isDigit(currentChar))
                accept();
        }
    }

    /* accept exponent mode digit */
    private void absorbExponent() {
        if ((currentChar == 'e' || currentChar == 'E') &&
                (((inspectChar(1) == '+' || inspectChar(1) == '-')
                        && Character.isDigit(inspectChar(2))) ||
                        Character.isDigit(inspectChar(1)))) {
            accept();
            accept();
            while (Character.isDigit(currentChar))
                accept();
        }
    }

    private void doCommentsignore() {
        switch (currentChar) {
            case '\t':
                // tab always pick most close position in right side
                currentChar = sourceFile.getNextChar();
                sourcePos.charStart += 8 - ((sourcePos.charStart - 1) % 8);
                break;
            case '\n':
                currentChar = sourceFile.getNextChar();
                sourcePos.charStart = 1;
                sourcePos.lineStart++;
                break;
            default:
                currentChar = sourceFile.getNextChar();
                sourcePos.charStart++;
                break;
        }
    }

    void skipSpaceAndComments() {
        while (true) {
            switch (currentChar) {
                case '/':
                    switch (inspectChar(1)) {
                        case '/':
                            doCommentsignore();
                            doCommentsignore();
                            while (currentChar != '\n' && currentChar != SourceFile.eof)
                                doCommentsignore();

                            break;
                        case '*':
                            int commentStrlneNo = sourcePos.lineStart;
                            int commentStrChrNo = sourcePos.charStart;
                            doCommentsignore();
                            doCommentsignore();
                            while (currentChar != '*' || inspectChar(1) != '/') {
                                if (currentChar == SourceFile.eof) {
                                    errorReporter.reportError("Unterminated comment", null,
                                            new SourcePosition(commentStrlneNo, commentStrChrNo,
                                                    commentStrChrNo));
                                    return;
                                } else
                                    doCommentsignore();
                            }
                            doCommentsignore();
                            doCommentsignore();
                            break;
                        default:
                            return;
                    }
                    break;
                case '\t':
                case '\n':
                case ' ':
                    doCommentsignore();
                    break;
                default:
                    return;
            }
        }
    }

    public Token getToken() {
        Token tok;
        int kind;
        sourcePos.charStart = sourcePos.charFinish + 1;
        // skip white space and comments
        skipSpaceAndComments();
        sourcePos.lineFinish = sourcePos.lineStart;
        sourcePos.charFinish = sourcePos.charStart - 1;

        currentSpelling = new StringBuffer("");
        // You must record the position of the current token somehow

        kind = nextToken();

        tok = new Token(kind, currentSpelling.toString(), sourcePos);

        // * do not remove these three lines
        if (debug)
            System.out.println(tok);
        return tok;
    }

}
