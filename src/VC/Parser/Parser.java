/*
 * Parser.java            
 *
 * This parser for a subset of the VC language is intended to 
 *  demonstrate how to create the AST nodes, including (among others): 
 *  [1] a list (of statements)
 *  [2] a function
 *  [3] a statement (which is an expression statement), 
 *  [4] a unary expression
 *  [5] a binary expression
 *  [6] terminals (identifiers, integer literals and operators)
 *
 * In addition, it also demonstrates how to use the two methods start 
 * and finish to determine the position information for the start and 
 * end of a construct (known as a phrase) corresponding an AST node.
 *
 * NOTE THAT THE POSITION INFORMATION WILL NOT BE MARKED. HOWEVER, IT CAN BE
 * USEFUL TO DEBUG YOUR IMPLEMENTATION.
 *
 * (09-|-April-|-2016)


program       -> func-decl
func-decl     -> type identifier "(" ")" compound-stmt
type          -> void
identifier    -> ID
// statements
compound-stmt -> "{" stmt* "}" 
stmt          -> expr-stmt
expr-stmt     -> expr? ";"
// expressions 
expr                -> additive-expr
additive-expr       -> multiplicative-expr
                    |  additive-expr "+" multiplicative-expr
                    |  additive-expr "-" multiplicative-expr
multiplicative-expr -> unary-expr
	            |  multiplicative-expr "*" unary-expr
	            |  multiplicative-expr "/" unary-expr
unary-expr          -> "-" unary-expr
		    |  primary-expr

primary-expr        -> identifier
 		    |  INTLITERAL
		    | "(" expr ")"
 */

package VC.Parser;

import VC.Scanner.Scanner;
import VC.Scanner.SourcePosition;
import VC.Scanner.Token;

import java.util.Stack;

import VC.ErrorReporter;
import VC.ASTs.*;

public class Parser {

	private Scanner scanner;
	private ErrorReporter errorReporter;
	private Token currentToken;
	private SourcePosition previousTokenPosition;
	private SourcePosition dummyPos = new SourcePosition();

	public Parser(Scanner lexer, ErrorReporter reporter) {
		scanner = lexer;
		errorReporter = reporter;

		previousTokenPosition = new SourcePosition();

		currentToken = scanner.getToken();
	}

	// match checks to see f the current token matches tokenExpected.
	// If so, fetches the next token.
	// If not, reports a syntactic error.

	void match(int tokenExpected) throws SyntaxError {
		if (currentToken.kind == tokenExpected) {
			previousTokenPosition = currentToken.position;
			currentToken = scanner.getToken();
		} else {
			syntacticError("\"%\" expected here", Token.spell(tokenExpected));
		}
	}

	void accept() {
		previousTokenPosition = currentToken.position;
		currentToken = scanner.getToken();
	}

	void syntacticError(String messageTemplate, String tokenQuoted) throws SyntaxError {
		SourcePosition pos = currentToken.position;
		errorReporter.reportError(messageTemplate, tokenQuoted, pos);
		throw (new SyntaxError());
	}

	// start records the position of the start of a phrase.
	// This is defined to be the position of the first
	// character of the first token of the phrase.

	void start(SourcePosition position) {
		position.lineStart = currentToken.position.lineStart;
		position.charStart = currentToken.position.charStart;
	}

	// finish records the position of the end of a phrase.
	// This is defined to be the position of the last
	// character of the last token of the phrase.

	void finish(SourcePosition position) {
		position.lineFinish = previousTokenPosition.lineFinish;
		position.charFinish = previousTokenPosition.charFinish;
	}

	void copyStart(SourcePosition from, SourcePosition to) {
		to.lineStart = from.lineStart;
		to.charStart = from.charStart;
	}

	// ========================== PROGRAMS ========================

	public Program parseProgram() {

		Program programAST = null;

		SourcePosition programPos = new SourcePosition();
		start(programPos);

		try {
			List dlAST = parseFuncOrValDecList();

			finish(programPos);
			programAST = new Program(dlAST, programPos);

			if (currentToken.kind != Token.EOF) {
				syntacticError("\"%\" unknown type", currentToken.spelling);
			}
		} catch (SyntaxError s) {
			return null;
		}
		return programAST;
	}

	// ========================== DECLARATIONS ========================

	List parseFuncOrValDecList() throws SyntaxError {
		SourcePosition pos = new SourcePosition();
		start(pos);
		List retDLst;
		Decl curDecl;
		List nextFuncOrVar;
		boolean isGlb = true;
		Stack<Decl> tmpStack = new Stack<Decl>();
		if (matchTypeFirstSet()) {
			Type type = parseType();
			Ident id = parseIdent();
			if (currentToken.kind == Token.LPAREN) {
				curDecl = parseFollowFunDecl(type, id);
				finish(pos);
				nextFuncOrVar = parseFuncOrValDecList();
			} else {
				curDecl = parseFollowVarDecl(type, id, isGlb);
				while (currentToken.kind == Token.COMMA) {
					accept();
					Decl decl = parseInitDeclarator(type, isGlb);
					tmpStack.push(decl);
				}
				match(Token.SEMICOLON);

				finish(pos);
				nextFuncOrVar = parseFuncOrValDecList();

				while(!tmpStack.isEmpty())
					nextFuncOrVar = new DeclList(tmpStack.pop(), nextFuncOrVar, pos);
					

			}

			retDLst = new DeclList(curDecl, nextFuncOrVar, pos);
		} else {
			finish(pos);
			retDLst = new EmptyDeclList(pos);
		}

		return retDLst;
	}

	Decl parseInitDeclarator(Type t, boolean isglb) throws SyntaxError {
		Decl retDecl = null;

		Expr exprInit = new EmptyExpr(dummyPos);
		Ident id = parseIdent();
		retDecl = parseFollowVarDecl(t, id, isglb);

		return retDecl;
	}

	List parseFuncDeclList() throws SyntaxError {
		List dlAST = null;
		Decl dAST = null;

		SourcePosition funcPos = new SourcePosition();
		start(funcPos);

		dAST = parseFuncDecl();

		if (matchTypeFirstSet()) {
			dlAST = parseFuncDeclList();
			finish(funcPos);
			dlAST = new DeclList(dAST, dlAST, funcPos);
		} else if (dAST != null) {
			finish(funcPos);
			dlAST = new DeclList(dAST, new EmptyDeclList(dummyPos), funcPos);
		}
		if (dlAST == null)
			dlAST = new EmptyDeclList(dummyPos);

		return dlAST;
	}

	Expr parseInitaliser() throws SyntaxError {
		InitExpr retExpr;
		Stack<Expr> tmpStack = new Stack<Expr>();
		SourcePosition pos = new SourcePosition();
		start(pos);
		if (currentToken.kind == Token.LCURLY) {
			accept();
			tmpStack.push(parseExpr());
			while (currentToken.kind == Token.COMMA) {
				accept();
				tmpStack.push(parseExpr());
			}
			match(Token.RCURLY);
		} else
			return parseExpr();

		finish(pos);
		ExprList elist = new ExprList(tmpStack.pop(), new EmptyExprList(dummyPos), pos);
		while(!tmpStack.isEmpty())
			elist = new ExprList(tmpStack.pop(),elist,pos);

		retExpr = new InitExpr(elist, pos);

		return retExpr;
	}

	Decl parseFuncDecl() throws SyntaxError {

		Decl fAST = null;

		SourcePosition funcPos = new SourcePosition();
		start(funcPos);

		Type tAST = parseType();
		Ident iAST = parseIdent();
		List fplAST = parseParaList();
		Stmt cAST = parseCompoundStmt();
		finish(funcPos);
		fAST = new FuncDecl(tAST, iAST, fplAST, cAST, funcPos);
		return fAST;
	}

	// ======================== TYPES ==========================

	Type parseType() throws SyntaxError {
		Type typeAST = null;

		SourcePosition typePos = new SourcePosition();
		start(typePos);
		switch (currentToken.kind) {
		case Token.VOID:
			typeAST = new VoidType(typePos);
			break;
		case Token.BOOLEAN:
			typeAST = new BooleanType(typePos);
			break;
		case Token.INT:
			typeAST = new IntType(typePos);
			break;
		case Token.FLOAT:
			typeAST = new FloatType(typePos);
			break;
		default:
			syntacticError("type expected hear", "");
		}
		accept();
		finish(typePos);

		return typeAST;
	}

	// ======================= STATEMENTS ==============================
	Stmt parseCompoundStmt() throws SyntaxError {
		Stmt cAST = null;
		SourcePosition stmtPos = new SourcePosition();
		start(stmtPos);
		match(Token.LCURLY);
		List dListAST = new EmptyDeclList(dummyPos);
		if (matchTypeFirstSet())
			dListAST = parseVarDeclList();
		List slAST = parseStmtList();
		match(Token.RCURLY);
		finish(stmtPos);

		if(dListAST instanceof EmptyDeclList && slAST instanceof EmptyStmtList)
			cAST = new EmptyCompStmt(dummyPos);
		else
			cAST = new CompoundStmt(dListAST, slAST, stmtPos);

		return cAST;
	}

	List parseVarDeclList() throws SyntaxError {
		SourcePosition pos = new SourcePosition();
		start(pos);
		boolean isGlb = false;
		Stack<Decl> stack = new Stack<Decl>();
		while (matchTypeFirstSet()) {
			Type type = parseType();
			Ident id = parseIdent();
			stack.push(parseFollowVarDecl(type, id, isGlb));
			while (currentToken.kind == Token.COMMA) {
				accept();
				Decl decl = parseInitDeclarator(type, isGlb);
				stack.push(decl);
			}
			match(Token.SEMICOLON);
		}

		finish(pos);

		DeclList ret = new DeclList(stack.pop(), new EmptyDeclList(dummyPos), pos);
		while(!stack.isEmpty())
			ret = new DeclList(stack.pop(), ret, pos);

			
		return ret;
	}

	List parseStmtList() throws SyntaxError {
		List slAST = null;
		SourcePosition stmtPos = new SourcePosition();
		start(stmtPos);

		if (currentToken.kind != Token.RCURLY) {
			Stmt sAST = parseStmt();
			{
				if (currentToken.kind != Token.RCURLY) {
					slAST = parseStmtList();
					finish(stmtPos);
					slAST = new StmtList(sAST, slAST, stmtPos);
				} else {
					finish(stmtPos);
					slAST = new StmtList(sAST, new EmptyStmtList(dummyPos), stmtPos);
				}
			}
		} else
			slAST = new EmptyStmtList(dummyPos);

		return slAST;
	}

	Stmt parseStmt() throws SyntaxError {
		Stmt sAST = null;

		switch (currentToken.kind) {
		case Token.CONTINUE:
			sAST = parseContinueStmt();
			break;
		case Token.LCURLY:
			sAST = parseCompoundStmt();
			break;
		case Token.IF:
			sAST = parseIfStmt();
			break;
		case Token.FOR:
			sAST = parseForStmt();
			break;
		case Token.WHILE:
			sAST = parseWhileStmt();
			break;
		case Token.BREAK:
			sAST = parseBreakStmt();
			break;
		case Token.RETURN:
			sAST = parseReturnStmt();
			break;
		default:
			sAST = parseExprStmt();
			break;
		}
		return sAST;
	}

	Stmt parseContinueStmt() throws SyntaxError
	{
		SourcePosition pos = new SourcePosition();
		start(pos);
		match(Token.CONTINUE);
		match(Token.SEMICOLON);
		finish(pos);
		return new ContinueStmt(pos);
	}

	Stmt parseIfStmt() throws SyntaxError
	{
		SourcePosition pos = new SourcePosition();
		start(pos);
		match(Token.IF);
		match(Token.LPAREN);
		Expr eAST = parseExpr();
		match(Token.RPAREN);
		Stmt s1AST = parseStmt();
		Stmt s2AST = new EmptyStmt(dummyPos);
		if (currentToken.kind == Token.ELSE) {
			accept();
			s2AST = parseStmt();
		}
		finish(pos);
		return new IfStmt(eAST, s1AST,s2AST, pos);
	}
	
	Stmt parseForStmt() throws SyntaxError
	{
		SourcePosition pos = new SourcePosition();
		start(pos);
		match(Token.FOR);
		match(Token.LPAREN);
		Expr e1AST = new EmptyExpr(dummyPos);
		Expr e2AST = new EmptyExpr(dummyPos);
		Expr e3AST = new EmptyExpr(dummyPos);
		if (matchExprFirstSet())
			e1AST = parseExpr();
		match(Token.SEMICOLON);
		if (matchExprFirstSet())
			e2AST = parseExpr();
		match(Token.SEMICOLON);
		if (matchExprFirstSet())
			e3AST = parseExpr();
		match(Token.RPAREN);
		Stmt sAST = parseStmt();
		finish(pos);
		
		return new ForStmt(e1AST, e2AST, e3AST, sAST, pos);
	}
	
	Stmt parseWhileStmt() throws SyntaxError
	{
		SourcePosition pos = new SourcePosition();
		start(pos);
		match(Token.WHILE);
		match(Token.LPAREN);
		Expr eAST = parseExpr();
		match(Token.RPAREN);
		Stmt sAST = parseStmt();
		finish(pos);
		return new WhileStmt(eAST, sAST, pos);
	}
	
	Stmt parseBreakStmt() throws SyntaxError
	{
		SourcePosition pos = new SourcePosition();
		start(pos);
		match(Token.BREAK);
		match(Token.SEMICOLON);
		finish(pos);
		return new BreakStmt(pos);
	}
	
	Stmt parseReturnStmt() throws SyntaxError
	{
		SourcePosition pos = new SourcePosition();
		start(pos);
		match(Token.RETURN);
		Expr eAST = null;
		if (matchExprFirstSet())
			eAST = parseExpr();
		else
			eAST = new EmptyExpr(dummyPos);
		match(Token.SEMICOLON);
		finish(pos);
		return new ReturnStmt(eAST, pos);
	}
	
//
	Stmt parseExprStmt() throws SyntaxError {
		Stmt sAST = null;

		SourcePosition stmtPos = new SourcePosition();
		start(stmtPos);

		if (matchExprFirstSet()) {
			Expr eAST = parseExpr();
			match(Token.SEMICOLON);
			finish(stmtPos);
			sAST = new ExprStmt(eAST, stmtPos);
		} else {
			match(Token.SEMICOLON);
			finish(stmtPos);
			sAST = new ExprStmt(new EmptyExpr(dummyPos), stmtPos);
		}
		return sAST;
	}

	// ======================= PARAMETERS =======================

	List parseParaList() throws SyntaxError {
		SourcePosition pos = new SourcePosition();
		start(pos);
		List retL = null;
		
		match(Token.LPAREN);
		if(currentToken.kind == Token.RPAREN) {
			accept();
			finish(pos);
			retL = new EmptyParaList(pos);
		}
		else
		{
			retL = parseProperParaList();
			match(Token.RPAREN);
			finish(pos);
		}
		
		return retL;
	}
	
	List parseProperParaList() throws SyntaxError {
		List retL = null;
		SourcePosition pos = new SourcePosition();
		start(pos);
		ParaDecl paraD = parseParaDecl();
		if(currentToken.kind == Token.COMMA)
		{
			accept();
			List nextL = parseProperParaList();
			finish(pos);
			retL = new ParaList(paraD, nextL, pos);
			
		}
		else
		{
			finish(pos);
			retL = new ParaList(paraD, 
					new EmptyParaList(dummyPos), pos);
		}
		
		return retL;
	}

	ParaDecl parseParaDecl() throws SyntaxError {
		SourcePosition pos = new SourcePosition();
		start(pos);
		Type t = parseType();
		Ident id = parseIdent();
		
		while (currentToken.kind == Token.LBRACKET) {
			accept();
			Expr intExpr = null;
			if (currentToken.kind == Token.INTLITERAL) {
				IntLiteral intL = parseIntLiteral();
				intExpr = new IntExpr(intL,previousTokenPosition);
				match(Token.RBRACKET);
			} 
			else
			{
				intExpr = new EmptyExpr(dummyPos);
				match(Token.RBRACKET);
			}
			finish(pos);
			t = new ArrayType(t, intExpr, pos) ;
		}

		return new ParaDecl(t, id, pos);
	}
	
	// ======================= EXPRESSIONS ======================

	Expr parseExpr() throws SyntaxError {
		Expr exprAST = null;
		exprAST = parseAssignmentExpr();
		return exprAST;
	}

	Expr parseAssignmentExpr() throws SyntaxError {
		Expr exprAST = null;
		SourcePosition pos = new SourcePosition();
		start(pos);

		exprAST = parseCondorExpr();
		if (currentToken.kind == Token.EQ) {
			accept();
			Expr secExpr = parseAssignmentExpr();
			finish(pos);
			exprAST = new AssignExpr(exprAST, secExpr, pos);
			return exprAST;
		}

		finish(pos);
		return exprAST;
	}

	Expr parseCondorExpr() throws SyntaxError {
		Expr exprAST = null;
		SourcePosition pos = new SourcePosition();
		start(pos);
		exprAST = parseCondandExpr();
		while (currentToken.kind == Token.OROR) {
			Operator o = acceptOperator();
			Expr secExpr = parseCondandExpr();
			finish(pos);
			exprAST = new BinaryExpr(exprAST, o, secExpr, pos);
		}

		return exprAST;
	}

	Expr parseCondandExpr() throws SyntaxError {
		Expr exprAST = null;
		SourcePosition pos = new SourcePosition();
		start(pos);
		exprAST = parseEqualityExpr();
		while (currentToken.kind == Token.ANDAND) {
			Operator o = acceptOperator();
			Expr secExpr = parseEqualityExpr();
			finish(pos);
			exprAST = new BinaryExpr(exprAST, o, secExpr, pos);
		}
		return exprAST;
	}

	Expr parseEqualityExpr() throws SyntaxError {
		SourcePosition pos = new SourcePosition();
		start(pos);
		Expr exprAST = parseRelExpr();
		while (currentToken.kind == Token.EQEQ || currentToken.kind == Token.NOTEQ) {
			Operator o = acceptOperator();
			Expr secExpr = parseEqualityExpr();
			finish(pos);
			exprAST = new BinaryExpr(exprAST, o, secExpr, pos);
		}
		return exprAST;
	}

	Expr parseRelExpr() throws SyntaxError {
		SourcePosition pos = new SourcePosition();
		start(pos);
		Expr exprAST = parseAdditiveExpr();
		while (currentToken.kind == Token.GT || currentToken.kind == Token.GTEQ || currentToken.kind == Token.LT
				|| currentToken.kind == Token.LTEQ) {
			Operator o = acceptOperator();
			Expr e2AST = parseAdditiveExpr();
			finish(pos);
			exprAST = new BinaryExpr(exprAST, o, e2AST, pos);
		}
		return exprAST;
	}

	Expr parseAdditiveExpr() throws SyntaxError {
		Expr exprAST = null;

		SourcePosition addStartPos = new SourcePosition();
		start(addStartPos);

		exprAST = parseMultiplicativeExpr();
		while (currentToken.kind == Token.PLUS || currentToken.kind == Token.MINUS) {
			Operator opAST = acceptOperator();
			Expr e2AST = parseMultiplicativeExpr();

			SourcePosition addPos = new SourcePosition();
			copyStart(addStartPos, addPos);
			finish(addPos);
			exprAST = new BinaryExpr(exprAST, opAST, e2AST, addPos);
		}
		return exprAST;
	}

	Expr parseMultiplicativeExpr() throws SyntaxError {

		Expr exprAST = null;

		SourcePosition multStartPos = new SourcePosition();
		start(multStartPos);

		exprAST = parseUnaryExpr();
		while (currentToken.kind == Token.MULT || currentToken.kind == Token.DIV) {
			Operator opAST = acceptOperator();
			Expr e2AST = parseUnaryExpr();
			SourcePosition multPos = new SourcePosition();
			copyStart(multStartPos, multPos);
			finish(multPos);
			exprAST = new BinaryExpr(exprAST, opAST, e2AST, multPos);
		}
		return exprAST;
	}

	Expr parseUnaryExpr() throws SyntaxError {

		Expr exprAST = null;

		SourcePosition unaryPos = new SourcePosition();
		start(unaryPos);

		switch (currentToken.kind) {
		case Token.MINUS:
		case Token.PLUS:
		case Token.NOT: {
			Operator opAST = acceptOperator();
			Expr e2AST = parseUnaryExpr();
			finish(unaryPos);
			exprAST = new UnaryExpr(opAST, e2AST, unaryPos);
		}
			break;

		default:
			exprAST = parsePrimaryExpr();
			break;

		}
		return exprAST;
	}

	Expr parsePrimaryExpr() throws SyntaxError {

		Expr exprAST = null;

		SourcePosition primPos = new SourcePosition();
		start(primPos);

		switch (currentToken.kind) {

		case Token.ID:
			Ident iAST = parseIdent();
			if (currentToken.kind == Token.LPAREN) {
				List argListAST = parseArgList();
				finish(primPos);
				exprAST = new CallExpr(iAST, argListAST, primPos);
			} else if (currentToken.kind == Token.LBRACKET) {
				Var arrayVar = new SimpleVar(iAST, previousTokenPosition);
				accept();
				Expr indexAST = parseExpr();
				match(Token.RBRACKET);
				finish(primPos);
				exprAST = new ArrayExpr(arrayVar, indexAST, primPos);
			} else {
				finish(primPos);
				Var simVAST = new SimpleVar(iAST, primPos);
				exprAST = new VarExpr(simVAST, primPos);
			}
			break;

		case Token.LPAREN: {
			accept();
			exprAST = parseExpr();
			match(Token.RPAREN);
		}
			break;

		case Token.INTLITERAL:
			IntLiteral ilAST = parseIntLiteral();
			finish(primPos);
			exprAST = new IntExpr(ilAST, primPos);
			break;
		case Token.FLOATLITERAL:
			FloatLiteral flAST = parseFloatLiteral();
			finish(primPos);
			exprAST = new FloatExpr(flAST, primPos);
			break;
		case Token.BOOLEANLITERAL:
			BooleanLiteral blAST = parseBooleanLiteral();
			finish(primPos);
			exprAST = new BooleanExpr(blAST, primPos);
			break;
		case Token.STRINGLITERAL:
			StringLiteral slAST = new StringLiteral(currentToken.spelling, primPos);
			exprAST = new StringExpr(slAST, primPos);
			match(Token.STRINGLITERAL);
			finish(primPos);
			break;
		default:
			syntacticError("illegal primary expression", currentToken.spelling);

		}
		return exprAST;
	}

	List parseArgList() throws SyntaxError {
		SourcePosition pos = new SourcePosition();
		List retLst = new EmptyArgList(dummyPos);
		start(pos);
		match(Token.LPAREN);
		if (matchExprFirstSet())
			retLst = parseProperArgList();
		match(Token.RPAREN);
		return retLst;
	}

	List parseProperArgList() throws SyntaxError {
		SourcePosition pos = new SourcePosition();
		start(pos);
		ArgList ret;
		Stack<Arg> stack = new Stack<Arg>();
		stack.push(parseArg());
		boolean inloop = false;
		while (currentToken.kind == Token.COMMA) {
			inloop = true;
			accept();
			stack.push(parseArg());
		}
		finish(pos);
		ret = new ArgList(stack.pop(), new EmptyArgList(dummyPos), pos);
		while(!stack.isEmpty())
			ret = new ArgList(stack.pop(),ret,pos);

		return ret;
	}

	Arg parseArg() throws SyntaxError {
		SourcePosition pos = new SourcePosition();
		start(pos);
		Expr expr = parseExpr();
		finish(pos);
		Arg ret = new Arg(expr, pos);
		return ret;
	}
	// ========================== ID, OPERATOR and LITERALS
	// ========================

	Ident parseIdent() throws SyntaxError {

		Ident I = null;

		if (currentToken.kind == Token.ID) {
			previousTokenPosition = currentToken.position;
			String spelling = currentToken.spelling;
			I = new Ident(spelling, previousTokenPosition);
			currentToken = scanner.getToken();
		} else
			syntacticError("identifier expected here", "");
		return I;
	}

	// acceptOperator parses an operator, and constructs a leaf AST for it

	Operator acceptOperator() throws SyntaxError {
		Operator O = null;

		previousTokenPosition = currentToken.position;
		String spelling = currentToken.spelling;
		O = new Operator(spelling, previousTokenPosition);
		currentToken = scanner.getToken();
		return O;
	}

	IntLiteral parseIntLiteral() throws SyntaxError {
		IntLiteral IL = null;

		if (currentToken.kind == Token.INTLITERAL) {
			String spelling = currentToken.spelling;
			accept();
			IL = new IntLiteral(spelling, previousTokenPosition);
		} else
			syntacticError("integer literal expected here", "");
		return IL;
	}

	FloatLiteral parseFloatLiteral() throws SyntaxError {
		FloatLiteral FL = null;

		if (currentToken.kind == Token.FLOATLITERAL) {
			String spelling = currentToken.spelling;
			accept();
			FL = new FloatLiteral(spelling, previousTokenPosition);
		} else
			syntacticError("float literal expected here", "");
		return FL;
	}

	BooleanLiteral parseBooleanLiteral() throws SyntaxError {
		BooleanLiteral BL = null;

		if (currentToken.kind == Token.BOOLEANLITERAL) {
			String spelling = currentToken.spelling;
			accept();
			BL = new BooleanLiteral(spelling, previousTokenPosition);
		} else
			syntacticError("boolean literal expected here", "");
		return BL;
	}

	Decl parseFollowFunDecl(Type type, Ident id) throws SyntaxError {
		Decl retD;
		SourcePosition pos = new SourcePosition();

		start(pos);

		List fplAST = parseParaList();
		Stmt cAST = parseCompoundStmt();

		finish(pos);
		retD = new FuncDecl(type, id, fplAST, cAST, pos);

		return retD;
	}

	Decl parseFollowVarDecl(Type type, Ident id, boolean isglb) throws SyntaxError {
		Decl retD;
		SourcePosition pos = new SourcePosition();
		GlobalVarDecl vAST;
		Expr expr = new EmptyExpr(dummyPos);
		Expr exprInit = new EmptyExpr(dummyPos);
		start(pos);

		if (currentToken.kind == Token.LBRACKET) {
			accept();
			if (currentToken.kind == Token.INTLITERAL) {
				IntLiteral intL = parseIntLiteral();
				expr = new IntExpr(intL, previousTokenPosition);

			}

			match(Token.RBRACKET);
			type = new ArrayType(type, expr, pos);
		}

		if (currentToken.kind == Token.EQ) {
			accept();
			exprInit = parseInitaliser();
		}

		finish(pos);
		retD = isglb ? new GlobalVarDecl(type, id, exprInit, pos) : new LocalVarDecl(type, id, exprInit, pos);

		return retD;
	}

	/* Lookahead for Type */
	private boolean matchTypeFirstSet() {
		switch (currentToken.kind) {
		case Token.VOID:
		case Token.BOOLEAN:
		case Token.INT:
		case Token.FLOAT:
			return true;
		default:
			return false;
		}
	}

	/* Lookahead for Expr */
	private boolean matchExprFirstSet() {
		switch (currentToken.kind) {
		case Token.LPAREN:
		case Token.PLUS:
		case Token.MINUS:
		case Token.NOT:
		case Token.ID:
		case Token.INTLITERAL:
		case Token.FLOATLITERAL:
		case Token.BOOLEANLITERAL:
		case Token.STRINGLITERAL:
			return true;
		default:
			return false;
		}
	}
}
